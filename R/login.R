library(RHTMLForms)
library(RCurl)
library(XML)

createStravaLogin = 
function(h = getCurlHandle(cookiejar = "", followlocation = TRUE),   
         u = 'https://www.strava.com/login')
{  
   txt = getURLContent(u, curl = h)
   doc = htmlParse(I(txt))
   docName(doc) = u
   
   fm = getHTMLFormDescription(doc)
   stravaLogin = createFunction(fm[[1]])
}





# StravaLogin = c(email = password)
if(FALSE) {
 h = getCurlHandle(cookiejar = "", followlocation = TRUE)
 stravaLogin = createStravaLogin(h)
 o = stravaLogin(names(StravaLogin), StravaLogin, .curl = h)
}

